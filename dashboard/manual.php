<?php
  $original_error="";
  if (isset($_POST['usdjpy_rate'])) {
    update_option('tel_number', wp_unslash($_POST['tel_number']));
    update_option('usdjpy_rate', wp_unslash($_POST['usdjpy_rate']));
    update_option('usdjpy_year', wp_unslash($_POST['usdjpy_year']));
    update_option('usdjpy_month', wp_unslash($_POST['usdjpy_month']));
    $original_error="更新されました";
  }
?>
<div class="manual-contents original_setting">
  <?php if($original_error!=""){ ?>
    <span class="caution"><?php echo $original_error;?></span>
  <?php } ?>
  <h1>設定</h1>
  <div class="container">
    <h2><span>設定</span></h2>
    <form class="" action="" method="post">
      <p>電話番号<br>
      <input type="text" name="tel_number" value="<?php form_option("tel_number");?>" class="telnumber"></p>
      <hr>
      <p class="input_name">USD/JPYレート<br>
      １ドル　<input type="text" name="usdjpy_rate" value="<?php form_option("usdjpy_rate");?>" class="usdjpy">円</p>
      <p class="input_name">レート取得年月<br>
      <input type="text" name="usdjpy_year" value="<?php form_option("usdjpy_year");?>" class="year">年<input type="text" name="usdjpy_month" value="<?php form_option("usdjpy_month");?>" class="month">月</p>
      <?php submit_button();?>
    </form>
  </div>
</div>
<style media="screen">
.original_setting .usdjpy,.original_setting form .year{
  width: 70px;
}
.original_setting form .month{
  width: 40px;
}
</style>


<div class="manual-contents">
    <h1>マニュアル</h1>
    <!--div class="container">
      <h2><span>ショートカット</span></h2>
      <?php get_template_part( 'manual/manual', 'shortcut' );?>
      <a href="https://www.welcart.com/documents/">Welcart公式マニュアル</a>
    </div-->

    <!--div class="container">
      <h2><span>商品登録時の注意点</span></h2>
      <div class="box">
        <h3>商品コード</h3>
        <p>商品コードは商品ごとに異なるものにしてください。</p>
        <p>SKU価格のSKUコードも価格ごとに異なるものにしてください。</p>
        <p>事業主用の価格設定は一般用のSKUコードの後ろに「_sp」をつけてください</p>
        <p>商品を登録する際は一般用と事業主用の二つづつ登録してください。</p>
        <h3>画像登録</h3>
        <p><a href="https://www.welcart.com/documents/manual-2/%e5%95%86%e5%93%81%e7%94%bb%e5%83%8f%e7%99%bb%e9%8c%b2">公式マニュアル</a></p>
        <p>サブ画像は商品コードと連番の間に_（アンダースコア）を2つ置く　例）test__01.jpg</p>
        <p>gifは</p>
      </div>
    </div-->
    <div class="container">
      <h2><span>申し込みフォーム</span></h2>
      <div class="box">
        <p><a href="<?php echo admin_url();?>admin.php?page=cfdb7-list.php">Contact Forms</a> -> [申込みフォーム]</p>
        <p>[Export CSV]からCSV書き出しできます</p>
        <div class="accbox">
          <label for="label1">項目詳細</label>
          <input type="checkbox" id="label1" class="cssacc" >
          <div class="accshow">
            <table>
              <tr><th>your-name</th><td>氏名</td></tr>
              <tr><th>your-kana</th><td>フリガナ</td></tr>
              <tr><th>birth-year</th><td>生年月日（年）</td></tr>
              <tr><th>birth-month</th><td>生年月日（月）</td></tr>
              <tr><th>birth-day</th><td>生年月日（日）</td></tr>
              <tr><th>your-sex</th><td>性別</td></tr>
              <tr><th>your-job</th><td>職業</td></tr>
              <tr><th>company-check</th><td>法人名を申込者として登録</td></tr>
              <tr><th>company-name</th><td>法人名</td></tr>
              <tr><th>your-tel</th><td>電話番号</td></tr>
              <tr><th>your-email</th><td>メールアドレス</td></tr>
              <tr><th>postal-code</th><td>郵便番号</td></tr>
              <tr><th>zip-1</th><td>住所（都道府県名）</td></tr>
              <tr><th>zip-2</th><td>住所（市区町村）</td></tr>
              <tr><th>zip-3</th><td>住所（番地）</td></tr>
              <tr><th>zip-4</th><td>住所（建物・その他）</td></tr>
              <tr><th>acceptance-248</th><td>利用規約</td></tr>
              <tr><th>course-select</th><td>お申し込みコース選択</td></tr>
              <tr><th>sending</th><td>発送方法</td></tr>
              <tr><th>finish</th><td>最大お預かり期間終了後</td></tr>
              <tr><th>b-price-1 ～ 10</th><td>Bコース選択（初期価格$）</td></tr>
              <tr><th>b-name-1 ～ 10</th><td>Bコース選択（商品名・特徴）</td></tr>
              <tr><th>c-price-1 ～ 10</th><td>Cコース選択（初期価格$）</td></tr>
              <tr><th>c-title-1 ～ 10</th><td>Cコース選択（タイトル）</td></tr>
              <tr><th>c-name-1 ～ 10</th><td>Cコース選択（商品名・特徴）</td></tr>
              <tr><th>description</th><td>説明文指定</td></tr>
              <tr><th>image</th><td>画像指定</td></tr>
              <tr><th>turn-tel</th><td>ウルベイからの電話連絡</td></tr>
              <tr><th>sonota</th><td>その他ご希望</td></tr>
              <tr><th>kiboday1</th><td>持ち込み日時（第一希望）</td></tr>
              <tr><th>kiboday2</th><td>持ち込み日時（第二希望）</td></tr>
              <tr><th>kiboday3</th><td>持ち込み日時（第三希望）</td></tr>
              <tr><th></th><td></td></tr>
              <tr><th></th><td></td></tr>
            </table>
          </div><!--.accshow end-->
        </div><!--.accbox end-->
      </div><!--.box end-->
    </div>
    <div class="container">
      <h2><span>オークション代行実績</span></h2>
      <div class="box">
        <p>新着順で表示されますので順番を入れ替えたい場合は「公開日時」で調整してください。</p>
        <p><a href="<?php echo admin_url();?>edit.php">投稿</a>より新規追加または編集してください。</p>
        <p>●タイトルを入力してください。</p>
        <p>●【カテゴリー】を　オークション代行実績　だけにチェックを入れてください。</p>
        <p>●【オークション代行実績】の項目を入力してください。</p>
      </div>
    </div>

    <div class="container">
      <h2><span>メール投稿</span></h2>
      <div class="box">
        <p><a href="<?php echo admin_url();?>admin.php?page=jetpack#/settings" >jetpack</a></p>
        <p>プラグイン「jetpack」のメール投稿機能を使っています。</p>
        <p><a href="mailto:teti284wevo@post.wordpress.com">teti284wevo@post.wordpress.com</a>（投稿用アドレス）</p>
        <ul>
            <li>メールのタイトルが投稿のタイトルになるので必ず入力してください。</li>
            <li>画像はメールに添付してください。</li>
            <li>デフォルトは「Blog」の投稿になります。</li>
            <li>「お知らせ」として投稿する場合はメール本文内に→　[category news]　←を入力してください。</li>
        </ul>
        <p class="kome">このアドレスは第三者に知られないようにしてください。jetpackの設定画面からアドレスを変更することが可能です。</p>

      </div>
    </div>

</div>
<style>
  .caution{
    color: red;
    font-weight: bold;
  }
  .manual-contents{
    padding-right: 20px;
  }
  .manual-contents,.manual-contents p{
    font-size: 16px;
  }
  .manual-contents p{
    margin:0 0 0.8rem;
  }
  .manual-contents h2 {
    line-height: 1.8;
    margin:0;
    margin-bottom: 0.8rem;
  }
  .manual-contents h2 span{
    border:solid 3px;
    border-radius: 20px;
    padding: 3px 5px;
  }
  .manual-contents h2 span:before,  .manual-contents h2 span:after{
    content:"★";
    font-size:1.5rem;
    margin:0 5px;
  }
  .manual-contents h3 {
    font-size: 1.1rem;
    border-bottom: solid 1px ;
    line-height: 1.5;
  }
  .manual-contents .container{
    margin-bottom: 2rem;
  }
  .manual-contents .box{
    padding:0 20px;
    margin-bottom:2em;
  }
  .chui{
    color: #ff327f;
    font-weight:bold;
  }
  ul.box,.box dl{
    margin: 0;
  }
  .box dt{
    font-weight: bold;
  }
  .box dd{
    margin-bottom: 15px;
  }
  .kome{
    font-size:95%;
    background:#ddd;
  }

  /*ボックス全体*/
  .accbox {
    max-width: 500px;
  }

  /*ラベル*/
  .accbox label {
      display: block;
      margin: 1.5px 0;
      padding : 11px 12px;
      color :#ddd;
      font-weight: bold;
      background :#23282d;
      cursor :pointer;
      transition: all 0.5s;
  }

  /*ラベルホバー時*/
  .accbox label:hover {
      background :#77838e;
  }

  /*チェックは隠す*/
  .accbox input {
      display: none;
  }

  /*中身を非表示にしておく*/
  .accbox .accshow {
      height: 0;
      padding: 0;
      overflow: hidden;
      opacity: 0;
      transition: 0.8s;
  }

  /*クリックで中身表示*/
  .cssacc:checked + .accshow {
      height: auto;
      padding: 5px;
      background: #eaeaea;
      opacity: 1;
  }
  th{
    text-align: left;
  }
</style>
