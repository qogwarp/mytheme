<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title" style="margin:0;">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">

		<div class="flow">
			<!--1-->
			<section class="step1">
			  <div class="step">
			    <h2 class="">1 お申込み</h2><p><span>お客様が行います。</span><span>費用０円</span></p>
			  </div>
			  <div class="flex">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_net.png" class="ico">インターネットでお申し込み</h3>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>order" class="btn">お申し込みフォーム</a></p>
				</div>
				<div class="flex">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_fax.png" class="ico">FAXでお申し込み</h3>
					<p><a href="" class="btn">FAX用紙ダウンロード</a></p>
				</div>
			</section>

			<div class="arrow"></div>

			<!--2-->
			<section class="step2">
			  <div class="step">
			    <h2>2 商品の梱包・発送</h2><p><span>お客様が行います。</span><span>費用０円</span></p>
			  </div>
			  <div class="box">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_haco.png" class="ico">無料で送る<span class="chui">（５点以上のご依頼に限ります。）</span></h3>
					<div class="indent">
						<p>お申し込み後、印字済みの着払い伝票をお送りします。<br>着払い伝票がウルベイから届きましたら、商品を梱包頂き、発送のご手配をお願いします。</p>
						<p><a href="https://mgr.post.japanpost.jp/C20P02Action.do;jsessionid=h2ZWX2hVsMmQbnLj6fFmx2tSlxlfCzQhzLyN7pVVvkdgZ2SQTSXB!121167365?ssoparam=0&termtype=0" target="_blank" class="btn">電話で集荷依頼</a>
						<a href="https://mgr.post.japanpost.jp/C20P02Action.do;jsessionid=h2ZWX2hVsMmQbnLj6fFmx2tSlxlfCzQhzLyN7pVVvkdgZ2SQTSXB!121167365?ssoparam=0&termtype=0" target="_blank" class="btn">インターネットで集荷依頼</a>
						<a href="https://www.post.japanpost.jp/shiten_search/" target="_blank" class="btn">郵便局へ直接持ち込む</a></p>
						<p class="chushaku">※コンビニ（ローソン、ミニストップ、セイコーマート、ローソンストア１００）への持ち込みも可能です。詳しくは、近隣店舗にてご確認ください。</p>
						<p class="chushaku">※梱包資材のご手配はお客様にてお願い申し上げます。再利用段ボールで構いません。商品が破損しないよう梱包をお願い致します。</p>
						※お手元に段ボールがない場合は、<a href="" target="_blank">郵便局</a>で注文が可能です。電話注文で無料配送となります。
					</div>
			  </div>
				<div class="box">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_haco.png" class="ico">元払いで送る</h3>
					<p class="indent">お客様ご自身でご発送いただく場合は、元払いとなります。</p>
				</div>
				<div class="box">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_track.png" class="ico">商品を持ち込む</h3>
					<p class="indent">直接お持ち込みも可能です。必ず事前お問い合わせ、ご予約をお願いします。</p>
				</div>
				<div class="box">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_track.png" class="ico">出張引き取り</h3>
					<p class="indent">東海・近畿・北陸地方にお住まいの方でご依頼品が大量にある場合は、出張引き取りも可能です。詳しくは、お問い合わせください。</p>
				</div>
			</section>

			<div class="arrow"></div>

			<!--3-->
			<section class="step3">
			  <div class="step">
			    <h2>3 商品の検品・出品</h2><p><span>ウルベイが行います。</span><span>費用０円</span></p>
			  </div>
			  <div class="box">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_pc.png" class="ico">商品をお受け取り後、お客様に代わり、オークション出品を行います。</h3>
					<div class="">
						<p class="indent">ウルベイが代行する業務</p>
						<ul class="flex waku">
							<li>・商品撮影</li>
							<li>・翻訳業務</li>
							<li>・適正カテゴリの選択</li>
							<li>・説明文作成</li>
							<li>・適正価格の決定</li>
							<li>・落札者とのやりとり</li>
							<li>・商品管理</li>
							<li>・質問対応</li>
							<li>・トラブル対応</li>
							<li>・梱包発送業務</li>
							<li>・入金確認</li>
						</ul>
					</div>
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_pc.png" class="ico">出品後はお送りするURLから出品・入札状況が確認できます。</h3>
				</div>
			</section>

			<div class="arrow"></div>

			<!--4-->
			<section class="step4">
			  <div class="step">
			    <h2>4 オークション終了</h2><p><span>ウルベイが行います。</span></p>
			  </div>
			  <div class="box">
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_hammer.png" class="ico">オークション終了後、ウルべイが入金確認・梱包・発送を行います。</h3>
					<p class="indent">未落札商品は、再出品もしくはYコースへの切り替えとなります。</p>
					<h3><img src="<?php echo get_template_directory_uri(); ?>/images/f_money.png" class="ico">ご精算</h3>
					<p class="indent">落札者様の荷物受け取り１週間後から、近い精算日にお振込みとなります。</p>
					<p class="waku indent">弊社規定精算日：毎月１５日、月末</p>
					<p class="chushaku indent">※規定日が土日、祝日の場合は、休み明けの平日が振込日となります。</p>
				</div>
			</section>


		</div>
		</div>
		</article><!-- #post-## -->
	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
