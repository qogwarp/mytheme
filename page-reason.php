<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area system">
	<main id="main" class="site-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title" style="margin:0;">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<img src="<?php echo get_template_directory_uri(); ?>/images/reason.png" alt="ウルベイが選ばれる理由">

			<div class="entry-content reason">
				<!-- 海外オークション手数料最安値水準　5％～ -->
				<section class="ouction">
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/reason_h_01.png" alt="海外オークション手数料最安値水準　5％～"></h2>
					<div class="">
						<img src="<?php echo get_template_directory_uri(); ?>/images/reason_ouction.png" alt="">
						<p>ウルベイでは業界最安値水準の手数料で価値あるサービスをお客様へご提供致します。</p>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>system">料金システム</a>
					</div>
				</section>

				<!-- 箱に詰めて送るだけ！お客様負担０円 -->
				<section class="hako">
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/reason_h_02.png" alt="箱に詰めて送るだけ！お客様負担０円"></h2>
					<div class="">
						<img src="<?php echo get_template_directory_uri(); ?>/images/reason_hako.png" alt="">
						<p>ウルベイから送られる着払い伝票使用で送料無料。<br>お客様が送料を負担する必要はありません。</p>
						<p class="chui">※５点以上のご利用の方に限ります。<br>※お客様指定の発送方法で送る場合は送料ご負担をお願いします。</p>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>flow">代行サービスの流れ</a>
					</div>
				</section>

				<!-- ebay最高評価ランクで集客抜群！ -->
				<section class="medal">
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/reason_h_03.png" alt="ebay最高評価ランクで集客抜群！"></h2>
					<div class="">
						<img src="<?php echo get_template_directory_uri(); ?>/images/reason_medal.png" alt="">
						<p>ウルベイはebayより最高評価ランク出品者として認定されています。<br>検索上位表示されることで、より落札の可能性が高まります。</p>
					</div>
				</section>

				<!-- 翻訳無料 -->
				<section>
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/reason_h_04.png" alt="翻訳無料"></h2>
					<div class="">
						<p>ウルベイスタッフが商品タイトル、説明文の英訳を行います。<br>また、バイヤーからの質問は和訳し、お客様へご連絡致します。</p>
						<p class="chui">※商品タイトル、説明文の翻訳はCコースのみ<br>※200文字以内に限ります。</p>
					</div>
				</section>

				<!-- こだわり出品にも柔軟対応 -->
				<section>
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/reason_h_05.png" alt="こだわり出品にも柔軟対応"></h2>
					<div class="">
						<p>「画像は用意したいが、英訳は行って欲しい」<br>「撮影は行って欲しいが、撮影箇所に指定がある」等<br>お客様と相談の上、ウルベイスタッフが柔軟に対応させて頂きます。</p>
						<p class="chui">※Cコースに限る</p>
					</div>
				</section>

				<!-- ヤフーオークションへの切替可能！ -->
				<section>
					<h2><img src="<?php echo get_template_directory_uri(); ?>/images/reason_h_06.png" alt="ヤフーオークションへの切替可能！"></h2>
					<div class="">
						<p>ebayで売れなかった場合でも、ヤフーオークションへ切替えて現金化が可能です。<br>国内オークションも経験豊富なウルベイスタッフへ是非お任せ下さい。</p>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>system#yahoomoney">料金システム</a>
					</div>
				</section>


			</div><!-- .entry-content -->

			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
						get_the_title()
					),
					'<footer class="entry-footer"><span class="edit-link">',
					'</span></footer><!-- .entry-footer -->'
				);
			?>

		</article><!-- #post-## -->


	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
