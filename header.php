<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', array(), '1.12.4');

	wp_head(); ?>
	<link rel='stylesheet' id='urubei-style-css'  href='<?php echo get_template_directory_uri();?>/urubeistyle.css' type='text/css' media='all' />
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script src="https://unpkg.com/glottologist"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/modaal/js/modaal.js"></script>
	<link rel='stylesheet' href='<?php echo get_template_directory_uri();?>/modaal/css/modaal.css' type='text/css' media='all' />
	<?php get_template_part('favicon'); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="site-inner">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>

		<header id="masthead" class="site-header" role="banner">
			<div class="site-header-main flex pc">
				<div class="site-branding">
					<?php //twentysixteen_the_custom_logo(); ?>

					<?php if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php echo get_template_directory_uri(); ?>/images/urubei_logo.png" alt="<?php bloginfo( 'name' ); ?>">
						</a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/urubei_logo.png" alt="<?php bloginfo( 'name' ); ?>"></a></p>
					<?php endif;?>
				</div><!-- .site-branding -->

				<div class="head_btn flex">
					<!-- PayPal Logo --><img src="https://www.paypalobjects.com/digitalassets/c/website/marketing/apac/jp/developer/BN-paypal-logo320_145.png" border="0" alt="PayPal（ペイパル）">
					<img src="<?php echo get_template_directory_uri(); ?>/images/head_tel.png" alt="052-766-6203">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>order" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/head_order.png" alt="オークション代行お申込みフォーム"></a>
					<a href="https://www.ebay.com/usr/urubei_japan" rel="home" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/head_ebay.png" alt="<?php bloginfo( 'name' ); ?> ebayストア"></a>
				</div>

			</div><!-- .site-header-main -->

		<nav class="pc head-nav" id="main-menu">
			<ul class="main-nav">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>first">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_1.png" alt="はじめての方" class="off">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_1on.png" alt="はじめての方" class="on">
				</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>flow">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_2.png" alt="代行サービスの流れ" class="off">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_2on.png" alt="代行サービスの流れ" class="on">
				</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>system">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_3.png" alt="料金" class="off">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_3on.png" alt="料金" class="on">
				</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>treatment">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_4.png" alt="取扱商品" class="off">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_4on.png" alt="取扱商品" class="on">
				</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>question">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_5.png" alt="よくある質問" class="off">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nav_5on.png" alt="よくある質問" class="on">
				</a></li>
				<li class="sp"><a href="<?php echo esc_url( home_url( '/' ) ); ?>order" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/sp_order.png" alt="オークション代行お申込みフォーム"></a></li>
			</ul>
		</nav>

		<!--sp_menu-->
		<div id="sp_header" class="sp">
			<div class="fix-nav flex">
				<div class="sp_nav_btn sp_tel_btn">
					<a href="tel:<?php echo str_replace("-", "", get_option('tel_number'));?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sp_tel.png" alt="<?php echo get_option('tel_number');?>"></a>
				</div>
				<?php if ( is_front_page() && is_home() ) : ?>
					<h1 class="sp-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri(); ?>/images/urubei_logo.png" alt="<?php bloginfo( 'name' ); ?>">
					</a></h1>
				<?php else : ?>
					<p class="sp-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/urubei_logo.png" alt="<?php bloginfo( 'name' ); ?>"></a></p>
				<?php endif;?>
				<div class="sp_nav_btn sp_menu_btn">
					<a href="#main-menu" class="modaal-open "><img src="<?php echo get_template_directory_uri(); ?>/images/sp_menu.png" alt="menu"></a>
				</div>
			</div>
			<script type="text/javascript">
			$(function() {
				$(".modaal-open").modaal({});
			});
			</script>
		</div>
		<!--sp_menu end-->

<?php if ( is_front_page() && is_home() ) : ?>
		<div class="top_content">
			<div class="top_content_in">
				<img src="<?php echo get_template_directory_uri(); ?>/images/top_content.png" alt="">
			</div>
		</div>
	<?php endif;?>

		<div class="top_head_b">
			<div class="head_b1">
				<img src="<?php echo get_template_directory_uri(); ?>/images/head_b1.png" alt="国内オークションへの切り替え対応">
			</div>
			<div class="head_b2">
				<img src="<?php echo get_template_directory_uri(); ?>/images/head_b2.png" alt="法人様専用オークション代行">
			</div>
		</div>

		</header><!-- .site-header -->

		<div id="content" class="site-content">
