<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area system">
	<main id="main" class="site-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title" style="margin:0;">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<div class="ebay">
					<section>
						<h2 class="sub_title">世界最大のオークションサイト「ebay(イーベイ)」とは？</h2>
						<p><img src="<?php echo get_template_directory_uri(); ?>/images/aboutebay.png" alt="ebayとは？"></p>
						<p><img src="<?php echo get_template_directory_uri(); ?>/images/aboutebay2.png" alt="世界中のユーザーと取引ができます"></p>
					</section>
				  <section>
				    <h2 class="sub_title">ebayとは?</h2>
				    <blockquote cite="https://ja.wikipedia.org/wiki/EBay">
				      eBay Inc.（イーベイ）は、インターネットオークションeBayを展開するアメリカ合衆国の会社である。世界中で1.6億人、Sellerは2,500万人（個人・法人含む）とインターネットオークションでは世界最多の利用者を持つ。
				    </blockquote>
						<p style="font-size:90%;text-align:right;">(<a href="https://ja.wikipedia.org/wiki/EBay">wikipedia</a>より)</p>
				  </section>

					<section>
						<h2 class="sub_title">どこの国に出品できるの？</h2>
						<p>ebayに出品すれば、全世界のユーザーが閲覧・購入が可能です。</p>
					</section>

					<section>
						<h2 class="sub_title">人気商品は何ですか？</h2>
						<p>ブランド品、カメラ、ゲーム、釣り具、アニメグッズ、車・バイクパーツ、アンティーク商品など、Made in Japanに代表される商品 です。</p>
					</section>


					<section>
						<h2 class="sub_title">落札相場を調べることはできますか？</h2>
						<p>はい、ebayでも国内オークション同様、落札相場を調べることが可能です。<br>詳しくは、サイドバー設置のebay落札相場検索をご覧ください。</p>
					</section>

					<section>
						<h2 class="sub_title">どんなものでも出品できますか？</h2>
						<p>国際法に反する商品などは出品できません。（生き物や危険品など）<br>詳しくは、<a href="<?php echo esc_url( home_url( '/' ) ); ?>treatment">取扱商品</a>のページをご覧ください。</p>
					</section>

					<section>
						<h2 class="sub_title">出品方法はどうなりますか？</h2>
						<p>タイトル（英語）、説明文（英語）、画像、送料設定（海外発送）、その他ebay上の設定をして出品致します。<br>ウルベイではオークション形式の出品のみ承っております。</p>
					</section>

					<section>
						<h2 class="sub_title">発送方法は何ですか？</h2>
						<p>日本郵政、DHL、FEDEX、UPS様のサービスを利用し、発送いたします。<br>※発送方法は弊社指定となります。</p>
					</section>

					<section>
						<h2 class="sub_title">支払はどのように受け取りますか？</h2>
						<p>ebay上の取引に関しては、全てpaypalを通じ、落札者から入金を受け取ります。<br>その後、お客様口座へ送金させていただきます。</p>
					</section>

				</div>

				<style media="screen">

				</style>

			</div><!-- .entry-content -->

			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
						get_the_title()
					),
					'<footer class="entry-footer"><span class="edit-link">',
					'</span></footer><!-- .entry-footer -->'
				);
			?>

		</article><!-- #post-## -->


	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
