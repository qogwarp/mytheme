$('#honyaku').on('click', function() {
  yaku();
});
function yaku(){
  const yaku = document.getElementById('hon'); //h1要素を取得
  const glot = new Glottologist();
  //h1要素のテキストを英語に翻訳して再びh1要素に上書きする
  glot.t( yaku.value, 'en' ).then( result => {
    //yaku.value = result;
    result=result.replace(/\s+/g, "+");
    url = 'https://www.ebay.com/sch/i.html?_from=R40&_nkw='+result+'&_in_kw=1&_ex_kw=&_sacat=0&LH_Sold=1&_udlo=&_udhi=&_samilow=&_samihi=&_sadis=15&_stpos=&_sargn=-1%26saslc%3D1&_fsradio2=%26LH_LocatedIn%3D1&_salic=104&LH_SubLocation=1&_sop=13&_dmd=1&_ipg=50&LH_Complete=1';
    window.open(url, '_blank');
  });
}
function uketori_result(){
  $(".souba_results").css("display","block");
  mikomi=$("#mikomi").val()-0;
  if($.isNumeric(mikomi) && mikomi!=0) {
    $(".nomoney,.course_y .chui").css("display","none");
    $(".results,.results .chushaku,.course_y .kekka").css("display","block");

    //入力金額　円
    if ($('input[name=unit]:checked').val()=="yen") {
      rate=$("#usdupyrate").html()-0;
      dol=mikomi/rate;
      abc_results2(dol);

      y_results(mikomi);

    }else {
    //入力金額　ドル
      abc_results2(mikomi);
      $(".results .chushaku").css("display","none");
      $(".course_y .kekka").css("display","none");
      $(".course_y .chui").css("display","block");

    }

  //入力がゼロもしくは数字以外
  }else{
    $(".nomoney").css("display","block");
    $(".results").css("display","none");

  }
}
function abc_results2(mikomi){
  var c_a=[
    [85,5,10],
    [82,8,10],
    [78,12,10],
    [75,15,10],
    [65,25,10]
  ];
  var c_b=[
    [83,7,10],
    [80,10,10],
    [75,15,10],
    [70,20,10],
    [60,30,10]
  ];
  var c_c=[
    [82,8,10],
    [78,12,10],
    [72,18,10],
    [68,22,10],
    [60,30,10]
  ];
  patern=100;
  if(mikomi>=3000){
    patern=0;
  }else if (mikomi>=1000) {
    patern=1;
  }else if(mikomi>=500){
    patern=2;
  }else if(mikomi>=100){
    patern=3;
  }else if(mikomi>=50){
    patern=4;
  }
  course=[".course_a",".course_b",".course_c"];
  c_rate=[c_a,c_b,c_c];
  if (patern!=100) {
    for(i=0;i<3;i++){
      $(course[i]+" .rate").html(c_rate[i][patern][1]+"%");
      $(course[i]+" .tesuryo .price").html(price2(mikomi,c_rate[i][patern][1]).toFixed(2)+"＄");
      $(course[i]+" .systemryo .price").html(price2(mikomi,c_rate[i][patern][2]).toFixed(2)+"＄");
      $(course[i]+" .uketori .price").html(price2(mikomi,c_rate[i][patern][0]).toFixed(2)+"＄");
    }
  }else{
    $(".course_a .rate,.course_b .rate,.course_c .rate").html("-");
    uk_price0=mikomi*0.9-15;
    uk_price=uk_price0.toFixed(2);
    tesu_price=15;
    system_price=mikomi*0.1;
    $(".course_a .tesuryo .price,.course_b .tesuryo .price,.course_c .tesuryo .price").html(tesu_price+"＄");
    $(".course_a .systemryo .price,.course_b .systemryo .price,.course_c .systemryo .price").html(system_price.toFixed(2)+"＄");
    $(".course_a .uketori .price,.course_b .uketori .price,.course_c .uketori .price").html(uk_price+"＄");
  }
}
function y_results(mikomi){
  var c_y=[
    [88,3,8.64],
    [86,5,8.64],
    [82,9,8.64],
    [77,14,8.64],
    [69,22,8.64],
    [55,36,8.64]
  ];
  patern_y=100;
  if(mikomi>=300000) {
    patern_y=0;
  }else if(mikomi>=100000){
    patern_y=1;
  }else if(mikomi>=50000){
    patern_y=2;
  }else if(mikomi>=20000){
    patern_y=3;
  }else if(mikomi>=5000){
    patern_y=4;
  }else if(mikomi>=3000){
    patern_y=5;
  }else if(mikomi>=1501){
    patern_y=10;
  }
  if (patern_y==100) {
    $(".course_y .rate").html("-");
    $(".course_y .tesuryo .price").html("-");
    $(".course_y .systemryo .price").html("-");
    $(".course_y .uketori .price").html("100円");
  }else if(patern_y==10){
    $(".course_y .rate").html("-");
    systemryo=price2(mikomi,8.64);
    uketori=mikomi-systemryo-1400;
    $(".course_y .tesuryo .price").html("1400円");
    $(".course_y .systemryo .price").html(Math.floor(systemryo).toLocaleString()+"円");
    $(".course_y .uketori .price").html(Math.floor(uketori).toLocaleString()+"円");
  }else{
    $(".course_y .rate").html(c_y[patern_y][1]+"%");
    $(".course_y .tesuryo .price").html(Math.floor(price2(mikomi,c_y[patern_y][1])).toLocaleString()+"円");
    $(".course_y .systemryo .price").html(Math.floor(price2(mikomi,c_y[patern_y][2])).toLocaleString()+"円");
    $(".course_y .uketori .price").html(Math.floor(price2(mikomi,c_y[patern_y][0])).toLocaleString()+"円");
  }
}
//mode => tesuryo or uketori;
function price(mikomikin,rate,mode){
  if(mode=="tesuryo"){
    return mikomikin*(100-rate)/100;
  }else if(mode=="uketori") {
    return mikomikin*rate/100;
  }
}
function price2(kingaku,rate){
  return kingaku*rate/100;
}

$(function() {
    unit_disp();
    $('input[name=unit]').change(function(){
      unit_disp();
    });
});
function unit_disp(){
  $('.s-money span').css('display','block');
  if ($('input[name=unit]:checked').val()=="yen") {
    $('.s-money .doll').css('display','none');
  }else {
    $('.s-money .yen').css('display','none');
  }
}
