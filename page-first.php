<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area system">
	<main id="main" class="site-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title" style="margin:0;">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content first">

				<div class="flex">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>ebay"><img src="<?php echo get_template_directory_uri(); ?>/images/wakaba_01.png" alt="世界最大のオークションサイト「ebay(イーベイ)」とは？"></a>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>reason"><img src="<?php echo get_template_directory_uri(); ?>/images/wakaba_03.png" alt="ウルベイが選ばえる理由"></a>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>flow"><img src="<?php echo get_template_directory_uri(); ?>/images/wakaba_04.png" alt="オークション代行サービスの流れ"></a>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>system"><img src="<?php echo get_template_directory_uri(); ?>/images/wakaba_05.png" alt="料金システムについて"></a>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>question"><img src="<?php echo get_template_directory_uri(); ?>/images/wakaba_06.png" alt="よくあるご質問"></a>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>treatment"><img src="<?php echo get_template_directory_uri(); ?>/images/wakaba_07.png" alt="取り扱い商品について"></a>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>treatment2"><img src="<?php echo get_template_directory_uri(); ?>/images/wakaba_08.png" alt="取扱不可商品"></a>
				</div>

			</div><!-- .entry-content -->

			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
						get_the_title()
					),
					'<footer class="entry-footer"><span class="edit-link">',
					'</span></footer><!-- .entry-footer -->'
				);
			?>

		</article><!-- #post-## -->


	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
