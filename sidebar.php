<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>


<aside id="secondary" class="sidebar" role="complementary">
	<div class="souba_container side_container">
		<h3><img src="<?php echo get_template_directory_uri();?>/images/calc0.png" alt="オークション代行受取金額シミュレーション" class="calc_pc">
		<img src="<?php echo get_template_directory_uri();?>/images/calc0-1.png" alt="オークション代行受取金額シミュレーション" class="calc_sp">
		</h3>
		<h4><span>1</span>出品したい商品のebay落札相場を検索</h4>
		<div class="">
			<div class="souba-box s-form flex">
				<input type="text" value="" id="hon"><button type="button" id="honyaku">相場検索</button>
			</div>
				<p class="chui">日本語で商品名・型番を入力</p>
		</div>

		<h4><span>2</span>見込み落札金額を入力</h4>
		<div class="souba-box s-unit flex">
			<label><input type="radio" name="unit" value="yen" class="unitselect" checked><span>円</span></label>
			<label><input type="radio" name="unit" value="doll" class="unitselect"><span>ドル</span></label>
		</div>
		<div class="souba-box s-money">
			<label for=""><input id=mikomi type="text" name="" value="" placeholder="0"><span class="yen">円</span><span class="doll">＄</span></label>
			<button type="button" name="button" onclick="uketori_result()">受取金額を計算▼</button>
		</div>

		<div class="souba-box souba_results">
		<h4>計算結果</h4>
		<div class="nomoney">
			金額を入力してください
		</div>
		<div class="results">
			<!--Aコース-->
			<section class="course_a">
				<h5>Aコース</h5>
				<div class="flex tesuryo">
					<p>手数料(<span class="rate"></span>)</p>
					<p class="price"></p>
				</div>
				<div class="flex systemryo">
					<p>ebay利用料(10%)</p>
					<p class="price"></p>
				</div>
				<div class="flex uketori">
					<p>お客様受取額</p>
					<p class="price"></p>
				</div>
			</section>
			<!--Bコース-->
			<section class="course_b">
				<h5>Bコース</h5>
				<div class="flex tesuryo">
					<p>手数料(<span class="rate"></span>)</p>
					<p class="price"></p>
				</div>
				<div class="flex systemryo">
					<p>ebay利用料(10%)</p>
					<p class="price"></p>
				</div>
				<div class="flex uketori">
					<p>お客様受取額</p>
					<p class="price"></p>
				</div>
			</section>
			<!--Cコース-->
			<section class="course_c">
				<h5>Cコース</h5>
				<div class="flex tesuryo">
					<p>手数料(<span class="rate"></span>)</p>
					<p class="price"></p>
				</div>
				<div class="flex systemryo">
					<p>ebay利用料(10%)</p>
					<p class="price"></p>
				</div>
				<div class="flex uketori">
					<p>お客様受取額</p>
					<p class="price"></p>
				</div>
			</section>
			<!--Yコース-->
			<section class="course_y">
				<h5>Yコース</h5>
				<div class="kekka">
					<div class="flex tesuryo">
						<p>手数料(<span class="rate"></span>)</p>
						<p class="price"></p>
					</div>
					<div class="flex systemryo">
						<p>ヤフオク利用料(8.64%)</p>
						<p class="price"></p>
					</div>
					<div class="flex uketori">
						<p>お客様受取額</p>
						<p class="price"></p>
					</div>
				</div>
				<div class="chui">
					日本円で入力してください
				</div>
			</section>
			<p class="chushaku">1ドル<span id="usdupyrate"><?php echo get_option("usdjpy_rate");?></span>円(<span id="usdupyrate"><?php echo get_option("usdjpy_year");?>/<span id="usdupyrate"><?php echo get_option("usdjpy_month");?>時点)</p>

			<a href="<?php echo esc_url( home_url( '/' ) ); ?>system">料金システムはこちら</a>
		</div><!--.results end-->

		</div>
	<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/mitsumori.js"></script>
	</div>
	<div class="side_container side_banner">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>reason"><img src="<?php echo get_template_directory_uri();?>/images/side_banner_reason.png" alt="ウルベイが選ばれる理由"></a>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>flow"><img src="<?php echo get_template_directory_uri();?>/images/side_banner_flow.png" alt="お客様負担ゼロ！とっても簡単な代行サービスの流れ"></a>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>ebay"><img src="<?php echo get_template_directory_uri();?>/images/side_banner_ebay.png" alt="世界最大のオークションサイト「ebay」をご存知ですか"></a>
		<a href="https://www.kaubei.com/" target="_blank"><img src="https://urubei.com/wp-content/uploads/2019/03/kaubei.jpg" width="450px"></a>
		<!--a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/images/side_banner.png"></a-->
		<a href="https://www.etradenagoya.com/" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/side_banner_etrade.png" alt="イートレード名古屋"></a>
		<a href="https://www.plan-international.jp/" target="_blank"><img src="https://urubei.com/wp-content/uploads/2019/03/side_banner_plan.gif" alt="PLAN" width="450px"></a>
	</div>
</aside><!-- .side_banner -->



<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<!--aside id="secondary" class="sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside--><!-- .sidebar .widget-area -->
<?php endif; ?>
