<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title">オークション代行実績商品詳細</h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<h2 class="sub_title"><?php echo get_the_title();?></h2>
		<?php
		$gazou = SCF::get('j_img');
		$gazou = wp_get_attachment_image_src($gazou,'large');
		$imgUrl = esc_url($gazou[0]);
		$rakusatsu = SCF::get('j_r_price');
		$nyusatsu = SCF::get('j_nyusatsu');

		?>
		<section class="jisseki">
			<div class="jisseki_img">
				<img src="<?php echo $imgUrl;?>" alt="">
			</div>
			<table>
				<tr>
					<th>商品名</th>
					<td><h3><?php echo get_the_title();?></h3></td>
				</tr>
				<tr>
					<th>価格</th>
					<td>￥<?php echo number_format($rakusatsu);?></td>
				</tr>
				<tr>
					<th>入札</th>
					<td><?php echo $nyusatsu;?></td>
				</tr>
			</table>
		</section>


	</div><!-- .entry-content -->

</article><!-- #post-## -->
