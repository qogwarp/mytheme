<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<footer>
			<article class="nemu flex">
				<section>
					<h3>オークション代行サービス</h3>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>first">はじめての方</a></p>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>flow">代行サービスの流れ</a></p>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>system">料金</a></p>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>treatment">取扱商品</a></p>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>treatment2">お取り扱いができない商品</a></p>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>order">お申し込みフォーム</a></p>
				</section>
				<section>
					<h3>ウルベイの強み</h3>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>reason">ウルベイが選ばれる理由</a></p>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>ebay">ebayとは？</a></p>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/blog">ブログ</a></p>
				</section>
				<section>
					<h3>出品・代行情報</h3>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>">オークション出品中商品一覧</a></p>
				</section>
				<section>
					<h3>ヘルプセンター</h3>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>question">よくあるご質問</a></p>
				</section>
			</article>
			<div class="foot-nav">
				<div class="">
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>info">会社概要</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>law">特定商取引法に基づく表記</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>rules">利用規約</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>recruit">採用案内</a></li>
					</ul>
					<p class="copy">Copyright © ウルベイ All Rights Reserved.</p>
				</div>
			</div>
		</footer>
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
