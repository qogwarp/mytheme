<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area system">
	<main id="main" class="site-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">

				<!--div class="course flex">
					<img src="<?php echo get_template_directory_uri();?>/images/4course_01.png" alt="">
					<img src="<?php echo get_template_directory_uri();?>/images/4course_02.png" alt="">
				</div>
				<h3 class="select_course">４つの出品コースが選べます</h3-->

				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					the_content();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}

					// End of the loop.
				endwhile;
				?>


			</div><!-- .entry-content -->
			<style media="screen">
				@media screen and (max-width: 480px) {
					.course{
						display: block;
					}
				}
				.select_course{
					text-align: center;
					background: #f0f0f0;
					line-height: 1.3;
					padding:8px 0;
					font-size: 18px;
					margin-bottom: 1.2em;
				}
				.entry-content .course_a h2{
					border-left-color:#fc7ba6;
				}
				.entry-content .course_b h2{
					border-left-color:#52a2f2;
				}
				.entry-content .course_c h2{
					border-left-color:#F4AE01;
				}
				.entry-content .course_y h2{
					border-left-color:#85B716;
				}
				.container strong{
					border:solid 2px #555;
					padding:10px 15px;
				}
			</style>
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
						get_the_title()
					),
					'<footer class="entry-footer"><span class="edit-link">',
					'</span></footer><!-- .entry-footer -->'
				);
			?>

		</article><!-- #post-## -->


	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
