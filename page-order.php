<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header(); ?>


<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>
		<div id="yesno0" style="display:none;">
			<p id=yesno>
				<img src="<?php echo get_stylesheet_directory_uri();?>/images/yesno.png" alt="">
			</p>
		</div>
		<style media="screen">
			.modaal-container{
				max-width: 600px;
			}
			/*
			form.wpcf7-form fieldset.fieldset-cf7mls:not(:first-of-type){
				height: auto;
				opacity: 1;
				visibility: visible;
			}*/
		</style>
		<script type="text/javascript">
			app="";
			var hiduke=new Date();
			var year = hiduke.getFullYear()-0;
			for (var i = year; i > year-100; i--) {
				app+='<option value="'+i+'">'+i+'</option>';
			}
			$("*[name=birth-year]").append(app);

			//NEXT、BACKボタン押したときに上にスクロール
			$(".entry-content button").on("click",function(){
				var position = $("#primary").offset().top;
				$(window).scrollTop(position);
			});

			//			course-select


			$(function(){
				$( 'input[name="course-select"]:radio' ).change( function() {
					var ycourse=$('input[name=course-select]:eq(3)');
					var finish=$('input[name=finish]:eq(0)');
					if(ycourse.prop('checked')){
						finish.prop('checked', false);
						finish.attr('disabled','disabled');
						finish.addClass('disabled');
					}else{
						finish.removeAttr('disabled');
						finish.removeClass('disabled');
					}
				});
				$('#kiyaku_btn').on('click',function(){
					window.open("<?php echo get_bloginfo('url');?>/rules", '_blank');
				});
			});
		</script>
		<style media="screen">
			.disabled+span,.disabled{
				display:none;
			}
			#c-detail .wpcf7-list-item{
				display: block;
			}
		</style>
	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
