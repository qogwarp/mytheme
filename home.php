<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<style media="screen">
.top_head_b{
	display: block;
}
.top_head_b > div{
	width: 100%;
	margin-bottom: 10px;
}
</style>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="top_lisseki top_container">
				<h2 class="entry-title">オークション代行実績</h2>
				<div class="j_container">

				<div class="slick">
	<?php
				$args = array('post_type' => 'post','cat'=>2);
				$the_query = get_posts( $args );
				foreach ( $the_query as $post ) : setup_postdata( $post );
				?>
						<div class="j_item">
							<a href="<?php echo get_permalink(); ?>">
								<?php $j_images = SCF::get('j_img'); echo wp_get_attachment_image( $j_images , 'true' ); ?>
								<h3><?php the_title();?></h3>
								<p class="j_u_price">受取金額：<span><?php echo number_format(post_custom('j_u_price'));?>円</span></p>
								<p class="j_r_price">落札金額：<span><?php echo number_format(post_custom('j_r_price'));?>円</span></p>
								<?php $rate = floor((post_custom('j_r_price')-post_custom('j_u_price'))*100/post_custom('j_r_price'));?>
								<p class="j_course">手数料率(<?php echo post_custom('j_course');?>)：<span><?php echo $rate;?>%</span></p>
							</a>
						</div>

					<?php	endforeach;?>
				<?php wp_reset_postdata();?>
			</div>
		</div>

<script type="text/javascript">
/*$('.slick').slick({
　　arrows: true,
　　slidesToShow:4,
　　prevArrow:'<div class="prev">PREV</div>',
　　nextArrow:'<div class="next">NEXT</div>'
　});
*/
$('.slick').slick({
 　　arrows: true,
 		autoplay:true,
		autoplaySpeed:5000,
		slidesToScroll:4,
  slidesToShow:4, //1024px以上のサイズに適用
 　　prevArrow:'<div class="prev"><span>▲</span></div>',
 　　nextArrow:'<div class="next"><span>▲</span></div>',
 responsive: [
{
      breakpoint: 768, // 480〜767px以下のサイズに適用
        settings: {
					slidesToScroll:3,
           slidesToShow: 3
         }
     },{
 breakpoint: 480, // 〜479px以下のサイズに適用
       settings: {
				 slidesToScroll:2,
          slidesToShow: 2
       }
    }
 ]
 });
</script>

			</div><!-- .jisseki end オークション代行実績　終わり -->

			<div class="top_news top_container">
				<h2 class="entry-title">ウルベイからのお知らせ</h2>

				<ul>
				<?php
							$args = array('post_type' => 'post','posts_per_page'=>8,'cat'=>3);
							$the_query = get_posts( $args );
							foreach ( $the_query as $post ) : setup_postdata( $post );
							?>
							<li><p><span class="date"><?php the_time('Y.m.d'); ?></span><a href="<?php echo get_permalink(); ?>"><?php the_title();?></a></p></li>
							<?php	endforeach;?>
					<?php wp_reset_postdata();?>
				</ul>
				<p style="text-align:right;"><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/news" class="btn">MORE ></a>
				</p>

				<style media="screen">
					.top_news li span.date{
						background: url("<?php echo get_template_directory_uri(); ?>/images/date_bg.png");
					}
				</style>

			</div>


		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
